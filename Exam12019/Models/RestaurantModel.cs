﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Models
{
    public class RestaurantModel
    {
        [Required]
        public Guid RestaurantID { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        [StringLength(2000)]
        public string Address { get; set; }

    }
}
