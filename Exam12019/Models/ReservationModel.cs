﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Models
{
    public class ReservationModel
    {
        [Required]
        public Guid ReservationID { get; set; }

        [Required]
        [StringLength(255),]
        public string Name { get; set; }

        [Required]
        [StringLength(255), EmailAddress]
        public string Email { get; set; }

        [Required]
        public DateTime ReservationTime { get; set; }

        [Required]
        public string RestaurantName { get; set; }

    }

    
}