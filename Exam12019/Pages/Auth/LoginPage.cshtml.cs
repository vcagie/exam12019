using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace Exam12019.Pages.Auth
{
    public class LoginPageModel : PageModel
    {

        [BindProperty]
        public UserModel FormUser { get; set; }

        public UserModel UserSetting { get; set; }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl, [FromServices]IConfiguration configuration)
        {

            UserSetting = new UserModel
            {
                Username = configuration["accelist"],
                Password = configuration["AdminPassword"]
            };

            if (ModelState.IsValid == false)
            {
                return Page();
            }

            var valid = FormUser.Username == UserSetting.Username && BCrypt.Net.BCrypt.Verify(FormUser.Password, UserSetting.Password);

            if (valid == false)
            {
                ModelState.AddModelError("FormUser.Password", "Invalid login username or password!");
                return Page();
            }

            var id = new ClaimsIdentity("ClaimIdentityLogin");
            id.AddClaim(new Claim(ClaimTypes.Email, FormUser.Username));
            id.AddClaim(new Claim(ClaimTypes.NameIdentifier, FormUser.Username));
            id.AddClaim(new Claim(ClaimTypes.Role, "Administrator"));

            var principal = new ClaimsPrincipal(id);
            await this.HttpContext.SignInAsync("ClaimIdentityLogin", principal, new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddYears(1),
                IsPersistent = true
            });

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToPage("/RestaurantViews/ViewAllRestaurant");
        }
    }
}