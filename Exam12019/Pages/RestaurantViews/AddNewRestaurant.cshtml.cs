using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.RestaurantViews
{
    [Authorize(Roles = "Administrator")]
    public class AddNewRestaurantModel : PageModel
    {
        private readonly IHttpClientFactory _HttpClientFactory;

        public AddNewRestaurantModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpClientFactory = httpClientFactory;
        }


        [BindProperty]
        public RestaurantModel RestaurantForm { get; set; }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var urlCreate = "http://localhost:50508/restaurant/create";
            var client = _HttpClientFactory.CreateClient();
            var response = await client.PostAsJsonAsync(urlCreate, new RestaurantModel
            {
                RestaurantID = Guid.NewGuid(),
                Name = RestaurantForm.Name,
                Address = RestaurantForm.Address
            });

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to add new restaurant");
            }

            return Page();

        }




    }
}