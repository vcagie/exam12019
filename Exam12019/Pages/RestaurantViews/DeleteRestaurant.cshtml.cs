using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.RestaurantViews
{
    public class DeleteRestaurantModel : PageModel
    {
        private readonly IHttpClientFactory _HttpClientFactory;

        public DeleteRestaurantModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpClientFactory = httpClientFactory;
        }
        [BindProperty(SupportsGet = true)]
        public Guid ID { set; get; }
        [BindProperty(SupportsGet = true)]
        public string Name { get; set; }

        public async Task OnGetAsync()
        {
            var urlCreate = "http://localhost:50508/restaurant/";
            var client = _HttpClientFactory.CreateClient();
            var response = await client.GetAsync(urlCreate + ID.ToString());

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to show restaurant!");
            }

            var restaurant = await response.Content.ReadAsAsync<RestaurantModel>();
            Name = restaurant.Name;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var urlCreate = "http://localhost:50508/restaurant/" + ID.ToString();
            var client = _HttpClientFactory.CreateClient();
            var response = await client.DeleteAsync(urlCreate);
            return RedirectToPage("/RestaurantViews/ViewAllRestaurant");
        }
    }
}