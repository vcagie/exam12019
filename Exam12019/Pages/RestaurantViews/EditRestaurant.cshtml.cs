using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.RestaurantViews
{
    [Authorize(Roles = "Administrator")]
    public class EditRestaurantModel : PageModel
    {
        private readonly IHttpClientFactory _HttpClientFactory;

        public EditRestaurantModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpClientFactory = httpClientFactory;
        }

        [BindProperty(SupportsGet = true)]
        public Guid ID { get; set; }

        [BindProperty]
        public RestaurantModel RestaurantForm { get; set; }

        public async Task OnGetAsync()
        {
            var urlCreate = "http://localhost:50508/restaurant/";
            var client = _HttpClientFactory.CreateClient();
            var response = await client.GetAsync(urlCreate + ID.ToString());

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to show restaurant!");
            }

            RestaurantForm = await response.Content.ReadAsAsync<RestaurantModel>();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var urlCreate = "http://localhost:50508/restaurant/";
            var client = _HttpClientFactory.CreateClient();
            var response = await client.PostAsJsonAsync(urlCreate + ID.ToString(), new RestaurantModel
            {
                RestaurantID = ID,
                Name = RestaurantForm.Name,
                Address = RestaurantForm.Address
            });

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Update Failed");
            }

            return RedirectToPage("/RestaurantViews/ViewAllRestaurant");

        }
    }
}