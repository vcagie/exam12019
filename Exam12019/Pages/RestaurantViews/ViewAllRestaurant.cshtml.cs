using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.RestaurantViews
{
    public class ViewAllRestaurantModel : PageModel
    {
        private readonly IHttpClientFactory _HttpClientFactory;

        public ViewAllRestaurantModel(IHttpClientFactory httpClientFactory)
        {
            _HttpClientFactory = httpClientFactory;
        }

        public List<RestaurantModel> AllRestaurants { get; set; } 

        public async Task OnGetAsync()
        {
            var urlCreate = "http://localhost:50508/restaurant";
            var client = _HttpClientFactory.CreateClient();
            var response = await client.GetAsync(urlCreate);

            if(response.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to show restaurant!");
            }

            AllRestaurants = await response.Content.ReadAsAsync<List<RestaurantModel>>();
            
        }
    }
}