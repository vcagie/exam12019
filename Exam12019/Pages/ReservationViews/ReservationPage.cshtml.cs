using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Exam12019.Pages.ReservationViews
{
    public class ReservationPageModel : PageModel
    {
        private readonly IHttpClientFactory _HttpClientFactory;

        [TempData]
        public string Message { set; get; }
        public ReservationPageModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpClientFactory = httpClientFactory;
        }

        [BindProperty]
        public ReservationModel ReservationForm { get; set; }

        private Guid ShowReservationID { get; set; }

        [BindProperty]
        static public List<SelectListItem> RestaurantNames { get; set; }

        public List<RestaurantModel> AllRestaurants { get; set; }

        public async Task OnGetAsync()
        {
            RestaurantNames = new List<SelectListItem>();
            var urlCreate = "http://localhost:50508/restaurant";
            var client = _HttpClientFactory.CreateClient();
            var response = await client.GetAsync(urlCreate);

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to show restaurant!");
            }

            AllRestaurants = await response.Content.ReadAsAsync<List<RestaurantModel>>();

            foreach (var restaurant in AllRestaurants)
            {
                RestaurantNames.Add(new SelectListItem
                {
                    Value = restaurant.RestaurantID.ToString(),
                    Text = restaurant.Name
                });
            }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            ShowReservationID = Guid.NewGuid();
            var urlCreate = "http://localhost:50508/reservation";
            var client = _HttpClientFactory.CreateClient();
            var response = await client.PostAsJsonAsync(urlCreate, new ReservationModel
            {
                ReservationID = ShowReservationID,
                Name = ReservationForm.Name,
                Email = ReservationForm.Email,
                ReservationTime = TimeZoneInfo.ConvertTimeToUtc(ReservationForm.ReservationTime), 
                RestaurantName = ReservationForm.RestaurantName
                
            });
            
            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to add new restaurant");
            }

            Message = "Success! Your Reservation ID is : " + ShowReservationID.ToString();
            return RedirectToPage();

        }
    }
}