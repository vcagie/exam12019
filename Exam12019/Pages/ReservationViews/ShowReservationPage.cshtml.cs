using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.ReservationViews
{
    public class ShowReservationPageModel : PageModel
    {
        private readonly IHttpClientFactory _HttpClientFactory;

        [BindProperty(SupportsGet = true)]
        public Guid IDReservasi { set; get; }

        public ReservationModel Reservasi { set; get; }

        public RestaurantModel RestaurantModel { set; get; }

        public ShowReservationPageModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpClientFactory = httpClientFactory;
        }

        public async Task<IActionResult> OnPost()
        {
            var urlCreate = "http://localhost:50508/reservation/";
            var client = _HttpClientFactory.CreateClient();
            var response = await client.GetAsync(urlCreate + IDReservasi.ToString());

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to show restaurant!");
            }

            Reservasi = await response.Content.ReadAsAsync<ReservationModel>();

            return Page();
        }

        //public async Task OnGetAsync()
        //{

        //}
    }
}