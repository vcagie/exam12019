﻿using Exam12019.Models;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class RestaurantServices
    {
        private readonly IDistributedCache _Cache;
        
        public RestaurantServices(IDistributedCache distributedCache)
        {
            this._Cache = distributedCache;
        }

        public async Task<List<RestaurantModel>> GetRestaurantAsync()
        {
            var valueJson = await _Cache.GetStringAsync("Restaurants");

            if(valueJson == null)
            {
                return new List<RestaurantModel>();
            }

            return JsonConvert.DeserializeObject<List<RestaurantModel>>(valueJson);
        }

        public async Task CreateRestaurantAsync(RestaurantModel restaurantModel)
        {
            var restaurants = await GetRestaurantAsync();

            if(restaurants == null)
            {
                restaurants = new List<RestaurantModel>();
            }

            restaurants.Add(restaurantModel);
            var jsonValue = JsonConvert.SerializeObject(restaurants);
            await _Cache.SetStringAsync("Restaurants", jsonValue);
        }

        public async Task UpdateRestaurantAsync(List<RestaurantModel> restaurant)
        {
            var valueJson = JsonConvert.SerializeObject(restaurant);
            await _Cache.SetStringAsync("Restaurants", valueJson);


        }

    }
}
