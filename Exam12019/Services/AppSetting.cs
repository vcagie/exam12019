﻿using Exam12019.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class AppSetting
    {
        public UserModel LoginUserValidation { get; set; }

        public string ReservationApiKey { get; set; }
    }
}
