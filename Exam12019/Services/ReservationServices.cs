﻿using Exam12019.Models;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class ReservationServices
    {
        private readonly IDistributedCache _Cache;

        public ReservationServices(IDistributedCache distributedCache)
        {
            this._Cache = distributedCache;
        }

        public async Task<List<ReservationModel>> GetReservationDataAsync()
        {
            var valueJson = await _Cache.GetStringAsync("Reservation");

            if (valueJson == null)
            {
                return new List<ReservationModel>();
            }

            return JsonConvert.DeserializeObject<List<ReservationModel>>(valueJson);
        }

        public async Task CreateReservationAsync(ReservationModel reservationModel)
        {
            var reservations = await GetReservationDataAsync();

            if (reservations == null)
            {
                reservations = new List<ReservationModel>();
            }

            reservations.Add(reservationModel);
            var jsonValue = JsonConvert.SerializeObject(reservations);
            await _Cache.SetStringAsync("Reservation", jsonValue);
        }



    }
}
