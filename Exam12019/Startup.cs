using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Exam12019
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddHttpClient();
            services
                .AddAuthentication("ClaimIdentityLogin")
                .AddCookie("ClaimIdentityLogin", option =>
                {
                    option.LoginPath = "/auth/LoginPage";
                    option.LogoutPath = "/auth/logoutpage";
                    option.AccessDeniedPath = "/auth/deniedpage";
                    //option.SessionStore = new RedisTicketStore(new Microsoft.Extensions.Caching.StackExchangeRedis.RedisCacheOptions
                    //{
                    //    Configuration = "localhost",
                    //    InstanceName = "BelajarAspLogin"
                    //});
                });

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = "localhost";
                options.InstanceName = "Exam12019";
            });

            //var cm = ConnectionMultiplexer.Connect("localhost");
            //services.AddDataProtection().PersistKeysToStackExchangeRedis(cm, "BelajarAspLoginEncryptionKey");

            services.AddScoped<RestaurantServices>();
            services.AddScoped<ReservationServices>();
            //services.AddSession();
            services.AddHttpContextAccessor();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc();
        }
    }
}
