﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Exam12019.API
{
    [Route("restaurant")]
    [ApiController]
    public class RestaurantApiController : ControllerBase
    {
        private readonly RestaurantServices _RestaurantService;

        public RestaurantApiController(RestaurantServices restaurantServices)
        {
            this._RestaurantService = restaurantServices;
        }

        // GET: api/ValuesApi
        [HttpGet(Name = "get-all-restaurant")]
        public async Task<List<RestaurantModel>> GetRestaurants()
        {
            var allRestaurant = await _RestaurantService.GetRestaurantAsync();
            return allRestaurant;
        }

        // GET: api/ValuesApi/5
        [HttpGet("{id}", Name = "get-restaurant-by-id")]
        public async Task<ActionResult<RestaurantModel>> GetRestaurantByID(Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return NotFound();
            }

            var allRestaurants = await _RestaurantService.GetRestaurantAsync();
            var searchRestaurant = allRestaurants.Where(Q => Q.RestaurantID == id).FirstOrDefault();

            if (searchRestaurant == null)
            {
                return NotFound();
            }

            return searchRestaurant;

        }

        // POST: api/ValuesApi
        [Route("create")]
        [HttpPost(Name = "create-new-restaurant")]
        public async Task<IActionResult> PostAsync([FromBody] RestaurantModel model)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            await _RestaurantService.CreateRestaurantAsync(model);
            return Ok();
        }

        [Route("edit")]
        [HttpPost("{id}", Name = "edit-restaurant-by-id")]
        public async Task<IActionResult> EditAsync(Guid id,[FromBody] RestaurantModel model)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var allRestaurants = await _RestaurantService.GetRestaurantAsync();
            var findRestaurant = allRestaurants.FindIndex(Q => Q.RestaurantID == id);

            if(findRestaurant == -1)
            {
                return NotFound();
            }

            allRestaurants[findRestaurant] = model;
            

            await _RestaurantService.UpdateRestaurantAsync(allRestaurants);
            return Ok();
        }

        // PUT: api/ValuesApi/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [Route("delete")]
        [HttpDelete("{id}", Name = "delete-restaurant-by-id")]
        public async Task<IActionResult> Delete(Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var allRestaurant = await _RestaurantService.GetRestaurantAsync();
            var removeRestaurant = allRestaurant.Where(Q => Q.RestaurantID == id).FirstOrDefault();

            allRestaurant.Remove(removeRestaurant);
            await _RestaurantService.UpdateRestaurantAsync(allRestaurant);
            return Ok();
        }
    }
}
