﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam12019.API
{
    [Route("reservation")]
    public class ReservationApiController : Controller
    {
        private readonly ReservationServices _ReservationServices;
        
        public ReservationApiController(ReservationServices reservationServices)
        {
            this._ReservationServices = reservationServices;
        }

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet("{id}", Name = "get-reservation-data")]
        public async Task<ActionResult<ReservationModel>> GetReservationByID(Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return NotFound();
            }

            var allReservation = await _ReservationServices.GetReservationDataAsync();
            var searchReservation = allReservation.Where(Q => Q.ReservationID == id).FirstOrDefault();

            if (searchReservation == null)
            {
                return null;
            }

            return searchReservation;
        }

        // POST api/<controller>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]ReservationModel model)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            await _ReservationServices.CreateReservationAsync(model);
            return Ok();
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
